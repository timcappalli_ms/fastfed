# ABSTRACT #

   FastFed simplifies the administrative effort to configure identity
   federation between an identity provider and a hosted application.
   
   The specification defines metadata documents, APIs, and flows to
   enable an administrator to quickly connect two providers that support
   common standards such as OpenID Connect, SAML, and SCIM, and allows
   configuration changes to be communicated directly between the
   identity provider and hosted application on a recurring basis.

# SPECIFICATION #

The draft specification is available as a text file in the source, at https://bitbucket.org/openid/fastfed/src
